/**
 * @author gucaobianco on 21 de dez de 2017
 *
 */

package processamentoImagens;

public class InformacoesImagem {
	private int[][] matrizPixels;
	private int[][] matrizAlpha;
	private int[][] matrizVermelho;
	private int[][] matrizVerde;
	private int[][] matrizAzul;
	private int altura;
	private int comprimento;
	
	public InformacoesImagem(int[][] matrizPixels, int[][] matrizAlpha, 
			int[][] matrizVermelho, int[][] matrizVerde, int[][] matrizAzul, 
			int altura, int comprimento) {
		this.matrizPixels = matrizPixels;
		this.matrizAlpha = matrizAlpha;
		this.matrizVermelho = matrizVermelho;
		this.matrizVerde = matrizVerde;
		this.matrizAzul = matrizAzul;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	public int[][] getMatrizPixels() {
		return matrizPixels;
	}
	public void setMatrizPixels(int[][] matrizPixels) {
		this.matrizPixels = matrizPixels;
	}
	public int[][] getMatrizAlpha() {
		return matrizAlpha;
	}
	public void setMatrizAlpha(int[][] matrizAlpha) {
		this.matrizAlpha = matrizAlpha;
	}
	public int[][] getMatrizVermelho() {
		return matrizVermelho;
	}
	public void setMatrizVermelho(int[][] matrizVermelho) {
		this.matrizVermelho = matrizVermelho;
	}
	public int[][] getMatrizVerde() {
		return matrizVerde;
	}
	public void setMatrizVerde(int[][] matrizVerde) {
		this.matrizVerde = matrizVerde;
	}
	public int[][] getMatrizAzul() {
		return matrizAzul;
	}
	public void setMatrizAzul(int[][] matrizAzul) {
		this.matrizAzul = matrizAzul;
	}
	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	public int getComprimento() {
		return comprimento;
	}
	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}
	public int getValorMatrizAlpha(int i, int j) {
		return matrizAlpha[i][j];
	}
	public int getValorMatrizVermelho(int i, int j) {
		return matrizVermelho[i][j];
	}
	public int getValorMatrizVerde(int i, int j) {
		return matrizVerde[i][j];
	}
	public int getValorMatrizAzul(int i, int j) {
		return matrizAzul[i][j];
	}	
	
	public void imprimeAzul() {
		for(int i=0; i < this.getComprimento(); i++) {
			for(int j=0; j< this.getAltura(); j++) {
				System.out.print(matrizAzul[i][j] + " ");
			}
			System.out.println(" ");
		}
	}
	
}
