/**
 * @author gucaobianco on 18 de dez de 2017
 *
 */

package processamentoImagens;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.imageio.ImageIO;

public class Inicio {
	public static BufferedImage abrirImagem(String nomeArquivo) {
		BufferedImage img = null;
		try {
			File imageFile = new File(
					"/home/gudev/Repositorios/processamentoImagens/src/processamentoImagens/" + nomeArquivo);
			img = ImageIO.read(imageFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (img != null) {
			System.out.println("A imagem foi aberta com sucesso. Abaixo, as informações da mesma.");
			System.out.println(img);
			return img;
		} else {
			return null;
		}

	}

	public static boolean salvarImagem(BufferedImage imagem, String nomeArquivo) {
		try {
			ImageIO.write(imagem, "png",
					new File(
							"/home/gudev/Repositorios/processamentoImagens/src/processamentoImagens/imagensProcessadas/"
									+ nomeArquivo + ".png"));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	// esse é o que escreve só a matriz principal com a+r+g+b
	public static void escreveEmArquivo(int[][] matriz, String nomeArquivo) { // esse metodo escreve em um arquivo txt
																				// as informacoes
		// tiradas da img
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
				"/home/gudev/Repositorios/processamentoImagens/src/processamentoImagens/" + nomeArquivo + ".txt"),
				"utf-8"))) {
			for (int i = 0; i < matriz.length; i++) {
				for (int j = 0; j < matriz[i].length; j++) {
					writer.write(matriz[i][j] + ", ");
				}

				writer.write("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static InformacoesImagem extrairDadosImagem(BufferedImage imagem) {
		if (imagem != null) {
			final byte[] pixels = ((DataBufferByte) imagem.getRaster().getDataBuffer()).getData();
			int altura = imagem.getHeight();
			int largura = imagem.getWidth();
			int red = 0, green = 0, blue = 0, alpha = 0;
			boolean temCanalAlpha = imagem.getAlphaRaster() != null;
			int[][] pixelArray = new int[altura][largura]; // cria a matriz que terá os valores
			int[][] redArray = new int[altura][largura];
			int[][] greenArray = new int[altura][largura];
			int[][] blueArray = new int[altura][largura];
			int[][] alphaArray = new int[altura][largura];

			if (temCanalAlpha) {
				final int qtdCanais = 4; // 4 canais, se RGB+A
				for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += qtdCanais) {
					int argb = 0;
					alpha = (((int) pixels[pixel] & 0xff) << 24); // alpha
					blue = ((int) pixels[pixel + 1] & 0xff); // blue
					green = (((int) pixels[pixel + 2] & 0xff) << 8); // green
					red = (((int) pixels[pixel + 3] & 0xff) << 16); // red
					argb = alpha + blue + green + red;
					pixelArray[row][col] = argb;
					redArray[row][col] = red;
					greenArray[row][col] = green;
					blueArray[row][col] = blue;
					alphaArray[row][col] = alpha;
					col++;

					if (col == largura) {
						col = 0;
						row++;
					}
				} // fim do loop for que percorre as colunas

			} else { // se entra nesse else, tem 3 canais apenas
				final int qtdCanais = 3;
				for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += qtdCanais) {
					int argb = 0;
					argb += -16777216; // 255 alpha
					argb += ((int) pixels[pixel] & 0xff); // blue
					argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
					argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
					pixelArray[row][col] = argb;
					col++;
					if (col == largura) {
						col = 0;
						row++;
					}

				}
			} // fim do else que checa se a imagem tem 3 canais

			InformacoesImagem info = new InformacoesImagem(pixelArray, alphaArray, redArray, greenArray, blueArray,
					altura, largura);
			return info;

		} else { // esse else significa que a abertura do arquivo deu problema
			System.out.println("ERRO!");
			return null;
		}

	}

	public static void pintarPretoImagem(BufferedImage imagem) {
		int altura = imagem.getHeight(), comprimento = imagem.getWidth();

		for (int i = 0; i < comprimento; i++) {
			for (int j = altura / 2; j < altura; j++) {
				imagem.setRGB(i, j, ((255 << 24) | (0 << 16) | (0 << 8) | 0));
			}

		}

		salvarImagem(imagem, "testePreto");

	}

	public static void extrairAzul(InformacoesImagem info, BufferedImage imagem) {
		for (int i = 0; i < imagem.getHeight(); i++) {
			for (int j = 0; j < imagem.getWidth(); j++) {
				imagem.setRGB(j, i, ((255 << 24) | (info.getValorMatrizVermelho(i, j) << 16)
						| (info.getValorMatrizVerde(i, j) << 8) | 0));
			}
		}

		boolean salvo = salvarImagem(imagem, "imagemSemAzul");
		if (salvo) {
			System.out.println("Salvo com sucesso");
		} else {
			System.out.println("nao foi salvo");
		}
	}

	public static void extrairVermelho(InformacoesImagem info, BufferedImage imagem) {
		for (int i = 0; i < imagem.getHeight(); i++) {
			for (int j = 0; j < imagem.getWidth(); j++) {
				imagem.setRGB(j, i, ((255 << 24) | (0 << 16) | (info.getValorMatrizVerde(i, j) << 8)
						| info.getValorMatrizAzul(i, j)));
			}
		}
		boolean salvo = salvarImagem(imagem, "imagemSemVermelho");
		if (salvo) {
			System.out.println("Salvo com sucesso");
		} else {
			System.out.println("nao foi salvo");
		}
	}

	public static void extrairVerde(InformacoesImagem info, BufferedImage imagem) {
		for (int i = 0; i < imagem.getHeight(); i++) {
			for (int j = 0; j < imagem.getWidth(); j++) {
				imagem.setRGB(j, i, ((255 << 24) | (info.getValorMatrizVermelho(i, j) << 16) | (0 << 8)
						| info.getValorMatrizAzul(i, j)));
			}
		}
		boolean salvo = salvarImagem(imagem, "imagemSemVerde");
		if (salvo) {
			System.out.println("Salvo com sucesso");
		} else {
			System.out.println("nao foi salvo");
		}
	}

	public static void extrairNegativo(InformacoesImagem info, BufferedImage imagem) {
		for (int i = 0; i < imagem.getHeight(); i++) {
			for (int j = 0; j < imagem.getWidth(); j++) {
				imagem.setRGB(j, i, ((255 << 24) | ((255 - info.getValorMatrizVermelho(i, j)) << 16)
						| ((255 - info.getValorMatrizVerde(i, j)) << 8) | ((255 - info.getValorMatrizAzul(i, j)))));

			}
		}
		boolean salvo = salvarImagem(imagem, "imagemNegativa");
		if (salvo) {
			System.out.println("Salvo com sucesso");
		} else {
			System.out.println("nao foi salvo");
		}
	}

	public static void main(String[] args) {
		BufferedImage imagem = abrirImagem("teste2.png");
		InformacoesImagem info = extrairDadosImagem(imagem);
		pintarPretoImagem(imagem);
		System.out.println("As informacoes da imagem são: ");
		System.out.println("Altura: " + info.getAltura() + ", Comprimento: " + info.getComprimento());

		extrairVermelho(info, imagem);
		extrairVerde(info, imagem);
		extrairAzul(info, imagem);
		extrairNegativo(info, imagem);
	}
}
